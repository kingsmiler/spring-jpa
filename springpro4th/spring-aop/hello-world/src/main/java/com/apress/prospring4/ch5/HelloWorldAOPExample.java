package com.apress.prospring4.ch5;

import org.springframework.aop.framework.ProxyFactory;

public class HelloWorldAOPExample {
    public static void main(String[] args) {
        MessageWriter target = new MessageWriter();

        ProxyFactory pf = new ProxyFactory();
        // 注册一个事件通知
        pf.addAdvice(new MessageDecorator());
        // 设置目标对象，目标对象的对应事件发生时都会发出通知
        pf.setTarget(target);

        MessageWriter proxy = (MessageWriter) pf.getProxy();

        target.writeMessage();
        System.out.println("");
        proxy.writeMessage();
        proxy.greeting();
    }
}
