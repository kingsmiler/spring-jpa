package net.petrikainulainen.spring.datajpa.repository;

import net.petrikainulainen.spring.datajpa.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Specifies methods used to obtain and modify person related information
 * which is stored in the database.
 *
 * @author robot wang
 */
public interface StudentRepository extends MyRepository<Student, Integer> {

    @Query("select u from Student u where u.name = ?1")
    Student getOneByName(String name);
}
