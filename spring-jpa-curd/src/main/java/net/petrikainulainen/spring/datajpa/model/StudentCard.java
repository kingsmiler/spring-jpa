package net.petrikainulainen.spring.datajpa.model;

import javax.persistence.*;

@Entity
public class StudentCard {
    private int id;
    private String name;
    //private Student student;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
    @OneToOne(mappedBy = "studentCard", cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    public Student getStudent() {
        // 由student 来维护
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;

    }
    */
}