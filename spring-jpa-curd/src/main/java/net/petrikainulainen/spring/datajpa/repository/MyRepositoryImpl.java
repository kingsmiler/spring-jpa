package net.petrikainulainen.spring.datajpa.repository;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author xman
 */
@SuppressWarnings("unchecked")
@NoRepositoryBean
public class MyRepositoryImpl<T, ID extends Serializable>
		extends SimpleJpaRepository<T, ID> implements MyRepository<T, ID> {

	private EntityManager entityManager;

	// There are two constructors to choose from, either can be used.
	public MyRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
		super(domainClass, entityManager);

		// This is the recommended method for accessing inherited class dependencies.
		this.entityManager = entityManager;
	}


	@Transactional
	public void deleteByIdList(Collection<ID> IdList) {
		List<T> entityList = new ArrayList<T>();
		for(ID id : IdList) {
			entityList.add(findOne(id));
		}

		deleteInBatch(entityList);
	}

	public void test(){

	}
}
