package net.petrikainulainen.spring.datajpa.controller;

import net.petrikainulainen.spring.datajpa.model.Student;
import net.petrikainulainen.spring.datajpa.model.StudentCard;
import net.petrikainulainen.spring.datajpa.repository.StudentCardRepository;
import net.petrikainulainen.spring.datajpa.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.persistence.criteria.*;
import java.awt.print.Pageable;

/**
 *
 */
@Controller
@RequestMapping(value = "/student")
public class StudentController extends AbstractController {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentCardRepository cardRepository;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public void add() {
        Student student = new Student() ;
        StudentCard studentCard = new StudentCard() ;
        student.setName("黄药师");
        student.setStudentCard(studentCard) ;
        studentCard.setName("001001010") ;

        studentRepository.save(student);
    }

    @RequestMapping(value = "/card", method = RequestMethod.GET)
    public void addCard() {
        String name = "黄药师";
        Student student = studentRepository.getOneByName(name);

        for(int i=1; i<5; i++) {
            StudentCard studentCard = new StudentCard();
            student.setStudentCard(studentCard);
            studentCard.setName("00100101" + i);
            studentRepository.save(student);
        }

        System.out.println(student);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public void list() {
        final String name = "黄药师";

        Specification<StudentCard> spec = new Specification<StudentCard>() {
            @Override
            public Predicate toPredicate(Root<StudentCard> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                final Subquery<Long> personQuery = query.subquery(Long.class);
                final Root<Student> person = personQuery.from(Student.class);
                final Join<Student, StudentCard> notes = person.join("studentCard");

                personQuery.select(notes.<Long> get("id"));
                personQuery.where(cb.equal(person.<String> get("name"), name));

                return cb.in(root.get("id")).value(personQuery);
            }
        };

        Page<StudentCard> studentPage = cardRepository.findAll(spec, new PageRequest(0, 10));
        System.out.println(studentPage);
    }


}
