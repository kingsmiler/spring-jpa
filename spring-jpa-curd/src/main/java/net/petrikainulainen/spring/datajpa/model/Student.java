package net.petrikainulainen.spring.datajpa.model;

import javax.persistence.*;

@Entity
public class Student {
    private int id;
    private String name;
    private StudentCard studentCard;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JoinColumn(name="student_card_id")
    @OneToOne(cascade={CascadeType.ALL})
    public StudentCard getStudentCard() {
        return studentCard;
    }

    public void setStudentCard(StudentCard studentCard) {
        this.studentCard = studentCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}