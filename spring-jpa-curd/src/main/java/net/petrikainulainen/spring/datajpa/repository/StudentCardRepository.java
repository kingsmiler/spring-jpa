package net.petrikainulainen.spring.datajpa.repository;

import net.petrikainulainen.spring.datajpa.model.Student;
import net.petrikainulainen.spring.datajpa.model.StudentCard;
import org.springframework.data.jpa.repository.Query;

/**
 * Specifies methods used to obtain and modify person related information
 * which is stored in the database.
 *
 * @author robot wang
 */
public interface StudentCardRepository extends MyRepository<StudentCard, Integer> {


}
